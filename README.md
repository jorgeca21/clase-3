# Ejercicio 0

Este ejercicio es para aprender a clonar y realizar pull requests con Bitbucket.

## Ejericio

Completa tu nombre acá abajo:

- Eduardo Diaz


## Referencias útiles

- Source Tree and Bitbucket: https://www.sourcetreeapp.com/#step-1
- Buenas prácticas para gestionar tu código: https://lnds.net/blog/lnds/2020/08/21/tres-buenas-practicas-para-gestionar-tu-codigo/
- El camino del backend developer: control de versiones: https://www.programando.org/blog/2022/02/20/el-camino-del-backend-developer-control-de-versiones/
- Video tutorial de Git y GitHub: https://www.youtube.com/watch?v=HiXLkL42tMU

